<?php

/**
 * @author 暮雨秋晨
 * @copyright 2014
 */

define('ROOT_DIR', dirname(__file__));
if (!file_exists('install.lock')) {
    header("Location:/install.php");
}
date_default_timezone_set('PRC');
require_once ROOT_DIR . DIRECTORY_SEPARATOR . 'DouPHP' . DIRECTORY_SEPARATOR .
    'DouPHP.php';
?>