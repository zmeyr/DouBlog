<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="暮雨秋晨" />
	<meta charset="UTF-8" />
	<title>DouBlog系统安装引导</title>
</head>
<body>
<?php

/**
 * @author 暮雨秋晨
 * @copyright 2014
 */

if (file_exists('install.lock')) {
    die('您已经安装过了，请不要重复安装。如果需要重复安装，请登录FTP手动删除根目录下面的.lock文件。');
}
if (isset($_POST['submit'])) {
    $host = $_POST['host'];
    $user = $_POST['user'];
    $pass = $_POST['pass'];
    $name = $_POST['name'];
    $pconnect = $_POST['pconnect'];
    $b_user = $_POST['b_user'];
    $b_pass = md5($_POST['b_pass'] . $_POST['b_user']);
    $mysql = mysql_connect($host, $user, $pass) or die('连接数据库失败');
    mysql_select_db($name, $mysql) or die('数据库不存在');
    mysql_query('SET NAMES `utf8`', $mysql);
    $config = '<?php
$db_host = \'' . $host . '\';
$db_name = \'' . $name . '\';
$db_user = \'' . $user . '\';
$db_pass = \'' . $pass . '\';
$db_charset = \'utf8\';
$db_pconnect = \'' . $pconnect . '\';
?>';
    if (file_put_contents('config/config.php', $config)) {
        echo ('写入配置项成功<br />');
    } else {
        die('无法写入数据库配置项，请检查config目录是否可写');
    }
    $sqls = explode(';', file_get_contents('doublog.sql'));
    echo ('总计 <b>' . count($sqls) . '</b> 句SQL语句<br />');
    foreach ($sqls as $key => $sql) {
        echo '执行第 <b>' . $key . '</b> 句中......';
        if (mysql_query($sql, $mysql)) {
            echo '<b>成功</b><br />';
        } else {
            echo '<b>失败</b><br />';
        }
    }
    echo ('初始化博客信息中......');
    $insert_config = "insert into config(title) values('欢迎使用DouBlog系统')";
    if (mysql_query($insert_config, $mysql)) {
        echo '成功<br />';
    } else {
        echo '失败<br />';
    }
    echo ('插入管理员信中......');
    $insert_master = "insert into master(user,pass) values('{$b_user}','{$b_pass}')";
    if (mysql_query($insert_master, $mysql)) {
        echo '成功<br />';
    } else {
        echo ('息失败<br />');
    }
    echo ('锁定安装状态中......');
    if (file_put_contents('install.lock', 'installed')) {
        echo ('成功');
    } else {
        echo ('失败');
    }
    echo ('<br />');
    echo ('删除数据库文件中......');
    if (unlink('doublog.sql')) {
        echo ('成功');
    } else {
        echo ('失败');
    }
    echo ('<br />');
    echo ('安装完毕，速度<a href="/">回到主页</a>查看吧、、、<br />');
} else {
?>
<form action="" method="post">
<h1>填写数据库信息</h1>
<label for="host">主机地址</label>
<input id="host" name="host" type="text" maxlength="64" value="localhost" /><br />
<label for="user">用户账号</label>
<input id="user" name="user" type="text" maxlength="24" placeholder="root" /><br />
<label for="pass">登录密码</label>
<input id="pass" name="pass" type="text" maxlength="32" placeholder="123456" /><br />
<label for="name">数据库名</label>
<input id="name" name="name" type="text" maxlength="24" placeholder="test" /><br />
<label for="pconnect">长连接</label>
<select name="pconnect"><option value="1" selected="selected">开启</option><option value="0">关闭</option></select><br />
<h2>填写博主信息</h2>
<label for="b_user">管理账号</label>
<input id="b_user" name="b_user" type="text" maxlength="64" value="localhost" /><br />
<label for="b_pass">后台密码</label>
<input id="b_pass" name="b_pass" type="text" maxlength="64" value="localhost" /><br />
<input type="submit" name="submit" value="连接并测试" />
</form>
  <?
}
?>
</body>
</html>