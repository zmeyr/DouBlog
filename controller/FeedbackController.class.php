<?php
class FeedbackController extends Controller
{
    private $db; //数据库对象
    function init()
    {
        require_once CONFIG . DS . 'config.php';
        $db = new Database('mysql', $db_host, $db_user, $db_pass, $db_name);
        $this->db = $db;
        $this->assign('css', '/view/' . TEMPLATE . '/static/css/main.css');
    }

    function reply()
    {
        LoadExt('client');
        $Client = new Client;
        $name = $_POST['name'];
        $email = $_POST['email'];
        $reply = $_POST['reply'];
        $aid = $_POST['aid'] + 0;
        if (empty($name) or empty($reply) or empty($email)) {
            $this->error('请将各个项目填写完整', 'javascript:history.go(-1)');
        }
        if (strlen($name) > 24 or strlen($email) > 192 or strlen($reply) > 768) {
            $this->error('长度超出限制范围，请不要非法提交数据', 'javascript:history.go(-1)');
        }
        if (!Filter_Mail($email)) {
            $this->error('邮件地址格式不正确', 'javascript:history.go(-1)');
        }
        if ($this->db->insert('reply', array(
            'pid' => 0,
            'aid' => $aid,
            'name' => $name,
            'email' => $email,
            'content' => $reply,
            'ptime' => time(),
            'ip' => $Client->getIp()))) {
            $this->success('发表成功', $Client->getRef());
        } else {
            $this->error('系统出错，请稍后重试', 'javascript:history.go(-1);');
        }
    }
}
?>