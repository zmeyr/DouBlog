<?php

class IndexController extends Controller
{
    private $db; //数据库对象
    function init()
    {
        require_once CONFIG . DS . 'config.php';
        $db = new Database('mysql', $db_host, $db_user, $db_pass, $db_name);
        $this->db = $db;
        $this->assign('css', '/view/' . TEMPLATE . '/static/css/main.css');
        $config = $db->getOneRecord('select * from config');
        $this->assign('footer', htmlspecialchars_decode($config['footer']));
    }
    function index()
    {
        $site = $this->db->getOneRecord('select * from config');
        $datas = $this->db->getOneRecord('select * from master');
        if (empty($datas['logo'])) {
            $datas['logo'] = '/upload/logo.jpg';
        }
        $this->assign('datas', $datas);
        $this->assign('title', $site['title']);
        $this->assign('desc', $site['desc']);
        $this->assign('keywd', $site['keywd']);
        $this->display();
    }

    function article()
    {
        $datas = $this->db->getAllRecord('select * from article order by id desc limit 0,10');
        $site = $this->db->getOneRecord('select * from config');
        $this->assign('title', '文章列表 - ' . $site['name']);
        $this->assign('datas', $datas);
        $this->display();
    }

    function photo()
    {
        $img_data = $this->db->getAllRecord('select * from image order by id desc limit 0,10');
        if (empty($img_data)) {
            $img_data = array();
        }
        $this->assign('datas', $img_data);
        $site = $this->db->getOneRecord('select * from config');
        $this->assign('title', '图片墙 - ' . $site['name']);
        $this->assign('desc', $site['desc']);
        $this->assign('keywd', $site['keywd']);
        $this->display();
    }

    function show()
    {
        if (isset($_GET['id'])) {
            $id = $_GET['id'] + 0;
            $data = $this->db->getOneRecord("select * from article where `id`='{$id}' limit 1",
                PDO::FETCH_ASSOC);
            $replys = $this->db->getAllRecord("select * from reply where aid={$id} order by id desc limit 0,10",
                PDO::FETCH_ASSOC);
            $this->db->update('article', array('hits' => $data['hits'] + 1), array(array(
                    'id',
                    '=',
                    $data['id'])));
            $data['hits'] += 1;
            $this->assign('title', $data['title'] . ' - 查阅文章');
            $this->assign('desc', $data['title']);
            $this->assign('keywd', $data['keywd']);
            $data['content'] = htmlspecialchars_decode($data['content']);
            $this->assign('data', $data);
            $this->assign('replys', $replys);
            $this->display();
        } else {
            $this->error('系统错误', $this->urlCreate('index', 'index'));
        }
    }

    function gossip()
    {
        $datas = $this->db->getAllRecord('select * from gossip order by id desc limit 0,10');
        $this->assign('datas', $datas);
        $site = $this->db->getOneRecord('select * from config');
        $this->assign('title', '闲言碎语 - ' . $site['name']);
        $this->assign('desc', $site['desc']);
        $this->assign('keywd', $site['keywd']);
        $this->display();
    }

    function link()
    {
        $links = $this->db->getAllRecord('select * from links order by id desc');
        $this->assign('links', $links);
        $site = $this->db->getOneRecord('select * from config');
        $this->assign('title', '友情链接 - ' . $site['name']);
        $this->assign('desc', $site['desc']);
        $this->assign('keywd', $site['keywd']);
        $this->display();
    }
}

?>