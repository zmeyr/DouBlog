<?php

/**
 * 博主专属控制器
 * @author 暮雨秋晨
 * @copyright 2014
 */

class MasterController extends Controller
{
    private $db = null;
    function init()
    {
        require_once CONFIG . DS . 'config.php';
        $db = new Database('mysql', $db_host, $db_user, $db_pass, $db_name);
        $this->db = $db;
        if ((!isset($_SESSION['login']) or empty($_SESSION['login'])) && $_GET['a'] !=
            'login') {
            $this->error('您还未登录，请登录', $this->urlCreate('master', 'login'));
        }
    }

    function index()
    {
        $this->display();
    }

    function login()
    {
        if (!isset($_POST['submit'])) {
            $this->display();
        } else {
            $u = $_POST['user'];
            $p = $_POST['pass'];
            if (empty($u) or empty($p)) {
                $this->error('账号密码不能留空', 'javascript:history.go(-1);');
            }
            $p = md5($p . $u);
            $data = $this->db->getOneRecord('select user,pass,name from master');
            if ($u == $data['user'] && $p == $data['pass']) {
                $_SESSION['login'] = $data;
                $name = empty($data['name']) ? '无名' : $data['name'];
                $this->success('欢迎回来，尊敬的 <b>' . $name . '</b> 博主', $this->urlCreate('master',
                    'index'));
            } else {
                $this->error('账号密码错误，请确认后再登录', 'javascript:history.go(-1);');
            }
        }
    }

    function center()
    {
        $this->display();
    }

    function article()
    {
        if (isset($_POST['submit'])) {
            $title = $_POST['title'];
            $content = $_POST['content'];
            $source = $_POST['source'];
            $keys = $_POST['keys'];
            if (empty($title) or empty($content)) {
                $this->error('文章标题或内容不能为空', 'javascript:history.go(-1);');
            }
            if ($this->db->insert('article', array(
                'title' => $title,
                'content' => nl2br($content),
                'keywd' => $keys,
                'ptime' => time(),
                'source' => $source))) {
                $this->success('发布成功', $this->urlCreate('master', 'center'));
            } else {
                $this->error('发布失败', 'javascript:history.go(-1);');
            }
        } else {
            $this->display();
        }
    }

    public function photo()
    {
        if (isset($_POST['submit'])) {
            $res = $this->upload();
            if ($res) {
                if ($this->db->insert('image', array(
                    'name' => $res['name'],
                    'path' => $res['path'],
                    'ptime' => time()))) {
                    $this->success('上传成功', $this->urlCreate('master', 'center'));
                    return true;
                } else {
                    @unlink($res['path']);
                    $this->error('上传失败', 'javascript:history.go(-1);');
                }
            } else {
                $this->error('系统错误，上传失败', 'javascript:history.go(-1);');
            }
        } else {
            $this->display();
        }
    }

    private function upload($field = 'upload')
    {
        LoadExt('upload'); //加载upload.class.php
        $upload = Upload::getInstance();
        $upload->setMaxSize(5242880); //最大5M
        $upload->setRootPath('upload');
        $upload->setPath(date("Y-m-d", time()));
        if ($res = $upload->run($field)) {
            return $res;
        } else {
            return false;
        }
    }

    public function gossip()
    {
        if (isset($_POST['submit'])) {
            $gossip = $_POST['gossip'];
            if (strlen($gossip) < 385) {
                if (!empty($gossip)) {
                    if ($this->db->insert('gossip', array('content' => $gossip, 'ptime' => time()))) {
                        $this->success('发布成功', $this->urlCreate('master', 'center'));
                    } else {
                        $this->error('发布失败', 'javascript:history.go(-1)');
                    }
                }
            } else {
                $this->error('字符数超过限制，请不要非法提交', 'javascript:history.go(-1)');
            }
        } else {
            $this->display();
        }
    }

    public function link()
    {
        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $url = $_POST['url'];
            $logo = $_POST['logo'];
            if (empty($name) || empty($url) || empty($logo)) {
                $this->error('请完整填写表单', 'javascript:history.go(-1);');
                exit;
            }
            if (!strpos($url, 'http://') && !strpos($url, 'https://')) {
                $url = 'http://' . $url;
            }
            if (!Filter_Url($url)) {
                $this->error('请输入一个正确的url', 'javascript:history.go(-1);');
            }
            if ($this->db->insert('links', array(
                'name' => $name,
                'url' => $url,
                'logo' => $logo,
                'ptime' => time()))) {
                $this->success('添加成功，快去主页查看吧', $this->urlCreate('master', 'center'));
                exit;
            } else {
                $this->error('系统出错了', 'javascript:history.go(-1);');
                exit;
            }
        } else {
            $this->display();
        }
    }

    public function logout()
    {

        session_destroy();
        $this->success('退出成功', '/');
    }

    public function config()
    {
        if (isset($_POST['submit'])) {
            $title = $_POST['title'];
            $name = $_POST['name'];
            $keywd = $_POST['keywd'];
            $desc = $_POST['desc'];
            $footer = $_POST['footer'];
            $url = $_POST['url'];
            if ($res = $this->db->update('config', array(
                'title' => $title,
                'name' => $name,
                'keywd' => $keywd,
                'desc' => $desc,
                'url' => $url,
                'footer' => $footer))) {
                $this->success('修改成功', $this->urlCreate('master', 'center'));
            } else {
                // dump($res);
                $this->error('系统出错，无法更新，请重试', 'javascript:history.go(-1)');
            }
        } else {
            $config = $this->db->getOneRecord('select * from config', PDO::FETCH_ASSOC);
            $this->assign('config', $config);
            $this->display();
        }
    }

    public function masterinfo()
    {
        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $age = $_POST['age'] + 0;
            $email = $_POST['email'];
            $qq = $_POST['qq'];
            $hobby = $_POST['hobby'];
            $introduce = $_POST['introduce'];
            $hometown = $_POST['hometown'];
            $skill = $_POST['skill'];
            $height = $_POST['height'];
            $signature = $_POST['signature'];
            if ($this->db->update('master', array(
                'name' => $name,
                'age' => $age,
                'email' => $email,
                'qq' => $qq,
                'hobby' => $hobby,
                'introduce' => $introduce,
                'hometown' => $hometown,
                'skill' => $skill,
                'height' => $height,
                'signature' => $signature))) {
                $this->success('修改成功', $this->urlCreate('master', 'center'));
            } else {
                $this->error('系统出错，无法修改，请重试', 'javascript:history.go(-1);');
            }
        } else {
            $data = $this->db->getOneRecord('select * from master');
            if (empty($data['logo'])) {
                $data['logo'] = '/upload/logo.jpg';
            }
            $this->assign('data', $data);
            $this->display();
        }
    }

    public function changelogo()
    {
        if (isset($_POST['submit'])) {
            $logo = $this->db->getOneRecord('select logo from master');
            $logo = $logo['logo'];
            if (empty($logo)) {
                $logo = 'upload/logo.jpg';
            }
            $res = $this->upload();
            if ($this->db->update('master', array('logo' => $res['path']))) {
                @unlink($logo);
                $this->success('修改成功', $this->urlCreate('master', 'center'));
            } else {
                @unlink($res['path']);
                $this->error('系统出错，未成功修改', 'javascript:history.go(-1);');
            }
        } else {
            $this->display();
        }
    }

    public function changepass()
    {
        if (isset($_POST['submit'])) {
            $pass = $_POST['pass'];
            $newpass = $_POST['newpass'];
            $cnewpass = $_POST['cnewpass'];
            $master = $this->db->getOneRecord('select user,pass from master');
            $pass = md5($pass . $master['user']);
            if ($newpass != $cnewpass) {
                $this->error('两次新密码输入不一致', 'javascript:history.go(-1);');
                exit;
            }
            if ($pass != $master['pass']) {
                $this->error('原始密码输入错误，请重新输入', 'javascript:history.go(-1);');
                exit;
            }
            $newpass = md5($newpass . $master['user']);
            if ($this->db->update('master', array('pass' => $newpass))) {
                $this->success('修改成功', $this->urlCreate('master', 'center'));
                exit;
            } else {
                $this->error('系统出错，修改失败', 'javascript:history.go(-1);');
            }
        } else {
            $this->display();
        }
    }
}

?>