﻿# Host: localhost  (Version: 5.5.38)
# Date: 2014-07-25 13:45:48
# Generator: MySQL-Front 5.3  (Build 4.128)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "article"
#

DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `content` longtext COMMENT '内容',
  `keywd` varchar(255) DEFAULT NULL COMMENT '关键字',
  `ptime` int(11) NOT NULL DEFAULT '0' COMMENT '发布时间',
  `source` varchar(6) NOT NULL DEFAULT '' COMMENT '来源',
  `hits` int(11) NOT NULL DEFAULT '0' COMMENT '点击量',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文章';

#
# Structure for table "config"
#

DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `keywd` varchar(255) DEFAULT NULL COMMENT '关键字',
  `desc` varchar(255) DEFAULT NULL COMMENT '描述',
  `url` varchar(255) DEFAULT NULL COMMENT '网址',
  `footer` longtext COMMENT '网站底部代码'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='网站设置';

#
# Structure for table "gossip"
#

DROP TABLE IF EXISTS `gossip`;
CREATE TABLE `gossip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) NOT NULL DEFAULT '' COMMENT '内容',
  `ptime` int(11) NOT NULL DEFAULT '0' COMMENT '发布时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='闲言碎语';

#
# Structure for table "image"
#

DROP TABLE IF EXISTS `image`;
CREATE TABLE `image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '文件名',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '完整路径',
  `ptime` int(11) NOT NULL DEFAULT '0' COMMENT '上传时间',
  `desc` varchar(255) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='图片墙';

#
# Structure for table "links"
#

DROP TABLE IF EXISTS `links`;
CREATE TABLE `links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '网址',
  `logo` varchar(255) DEFAULT '' COMMENT 'logo地址',
  `ptime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='友情链接';

#
# Structure for table "master"
#

DROP TABLE IF EXISTS `master`;
CREATE TABLE `master` (
  `user` varchar(255) NOT NULL DEFAULT '' COMMENT '登录账户',
  `pass` varchar(255) NOT NULL DEFAULT '' COMMENT '登录密码',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '昵称',
  `age` int(11) NOT NULL DEFAULT '0' COMMENT '年龄',
  `email` varchar(255) NOT NULL DEFAULT '' COMMENT '邮箱',
  `qq` varchar(255) DEFAULT NULL COMMENT 'QQ',
  `hobby` varchar(255) DEFAULT NULL COMMENT '个人爱好',
  `hometown` varchar(128) DEFAULT NULL COMMENT '家乡',
  `skill` varchar(255) DEFAULT NULL COMMENT '技术',
  `height` int(3) DEFAULT '0' COMMENT '身高',
  `signature` varchar(255) DEFAULT NULL COMMENT '个性签名',
  `logo` varchar(255) DEFAULT NULL COMMENT '头像存放路径',
  `introduce` longtext COMMENT '个人介绍'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='博主信息';

#
# Structure for table "reply"
#

DROP TABLE IF EXISTS `reply`;
CREATE TABLE `reply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父级ID',
  `aid` int(11) NOT NULL DEFAULT '0' COMMENT '文章ID',
  `content` longtext NOT NULL COMMENT '回复内容',
  `ptime` int(11) NOT NULL DEFAULT '0' COMMENT '回复时间',
  `ip` varchar(255) NOT NULL DEFAULT '0' COMMENT '发言者IP',
  `email` varchar(255) NOT NULL DEFAULT '' COMMENT '发言者邮箱',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='回复';
