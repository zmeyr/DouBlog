<?php defined('ROOT_DIR') or die('Access Denied'); ?>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="暮雨秋晨" />
  <meta name="description" content="<?php echo isset($desc) ? $desc : ''; ?>" />
  <meta name="keywords" content="<?php echo isset($keywd) ? $keywd : ''; ?>" />
  <link rel="stylesheet" type="text/css" href="<?php echo isset($css) ? $css :
''; ?>" />
  <?php if (isset($meta_data) && is_array($meta_data) && !empty($meta_data)) {
    foreach ($meta_data as $meta) {
        echo ($meta . "\r\n");
    }
} ?>
	<title><?= $title ?></title>
</head>
