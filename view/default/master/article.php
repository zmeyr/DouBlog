<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="暮雨秋晨" />
  <style>
  h1{
    text-align: center;
  }
  #main{
    width: 80%;
    max-width: 700px;
    margin: 0 auto;
    margin-bottom: 10%;
  }
  textarea{
    width: 700px;
    height: 480px;
  }
  </style>
  <script type="text/javascript" src="/view/default/static/js/nicEdit-latest.js"></script>
  <script type="text/javascript">
//<![CDATA[
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
  //]]>
  </script>
</head>
<body>
<h1>撰写文章</h1>
<form action="" method="post">
<div id="main">
<div class="article_title" style="font-size: 24px;margin-bottom: 3px;">标题：<span><input style="width: 80%;outline: none;height: 24px;line-height: 24px;font-size: 18px;padding-left: 3px;" type="text" name="title" maxlength="64" /></span></div>
<div>文章来源：<select name="source"><option value="原创" selected="selected">原创</option><option value="转载">转载</option><option value="引用">引用</option></select>&nbsp;关键字：<span><input type="text" maxlength="32" name="keys" style="width: 50%;outline: none;" /></span></div>
<div class="article_content"><textarea name="content" style="overflow: auto;padding: 2px;resize: none;outline: none;padding-left: 3px;word-wrap: break-word;word-break: break-all;"></textarea></div>
<div class="btn" style="text-align: center;margin-top: 6px;"><input style="width: 50%;max-width: 128px;height: 32px;font-size: 24px;" type="submit" name="submit" value="确定发布" /></div>
</div>
</form>
</body>
</html>