<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="暮雨秋晨" />
  <style>
  h1{
    text-align: center;
  }
  div#main{
    width: 50%;
    max-width: 700px;
    margin: 0 auto;
  }
  div#main div{
    margin: 6px;
    width: 80%;
    text-align: left;
  }
  div#main div label{
    font-weight: bold;
    margin-right: 6px;
  }
  input{
    outline: none;
    padding-left: 3px;
  }
  .margin_left{
    margin-left: 1em !important;
  }
  .center{
    margin-left: 10%;
  }
  </style>
</head>
<body>
<h1>修改密码</h1>
<div id="main">
<form action="" method="post">
<div><label for="pass" class="margin_left">原密码</label><input type="text" name="pass" id="pass" maxlength="128" /></div>
<div><label for="newpass" class="margin_left">新密码</label><input type="text" name="newpass" id="newpass" maxlength="128" /></div>
<div><label for="cnewpass">重复输入</label><input type="text" name="cnewpass" id="cnewpass" maxlength="128" /></div>
<input type="submit" name="submit" value="确定修改" class="center" />
</form>
</div>
</body>
</html>