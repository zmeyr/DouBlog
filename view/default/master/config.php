<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="暮雨秋晨" />
  <style>
  h1{
    text-align: center;
  }
  #main{
    text-align: center;
  }
  #main div{
    margin: 6px 3px;
    text-align: left;
    width: 100%;
  }
  #main div label{
    font-weight: bold;
    height: 24px;
    line-height: 24px;
    font-size: 18px;
    width: 20%;
  }
  #main div label span{
    font-weight: lighter;
    font-size: 16px;
    margin-left: 3px;
  }
  #main div input{
    height: 18px;
    line-height: 18px;
    font-size: 16px;
    width: 80%;
    outline: none;
  }
  #main div textarea{
    width: 80%;
    height: 120px;
    resize: none;
    outline: none;
    overflow-x: hidden;
    overflow-y: auto;
    word-break: break-all;
    word-wrap: break-word;
  }
  </style>
</head>
<body>
<h1>博客设置</h1>
<div id="main" style="width: 50%;max-width: 700px;margin: 0 auto;border: 1px solid #ccc;background-color: white;">
<form action="" method="post">
<div><label for="title">网站标题<span>(展示在首页的标题)</span></label><br /><input type="text" name="title" id="title" maxlength="64" value="<?=$config['title']?>" /></div>
<div><label for="name">网站名称<span>(用于链接交换、展示在内页标题上)</span></label><br /><input type="text" name="name" id="name" maxlength="12" value="<?=$config['name']?>" /></div>
<div><label for="url">网站URL<span>(访问网站使用的网址)</span></label><br /><input type="text" name="url" id="url" maxlength="128" value="<?=$config['url']?>" /></div>
<div><label for="keywd">关键字<span>(网站全局关键字，用于SEO优化)</span></label><br /><input type="text" name="keywd" id="keywd" maxlength="256" value="<?=$config['keywd']?>" /></div>
<div><label for="desc">博客描述<span>(主要是首页描述，用于SEO优化)</span></label><br /><textarea id="desc" name="desc"><?=$config['desc']?></textarea></div>
<div><label for="footer">网站底部<span>(网站底部代码，可以加流量统计、版权信息之类)</span></label><br /><textarea id="footer" name="footer"><?=$config['footer']?></textarea></div>
<input type="submit" name="submit" value="确定修改" />
</form>
</div>
</body>
</html>