<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="暮雨秋晨" />
  <style>
  body{
    margin-top: 30px;
    margin-bottom: 90px;
  }
  h1{
    text-align: center;
  }
  #main{
    width: 50%;
    max-width: 700px;
    margin: 0 auto;
    border: 1px solid #ccc;
    background-color: white;
  }
  #main div{
    margin: 6px 3px;
    text-align: left;
    width: 100%;
  }
  #main div label{
    font-weight: bold;
    height: 24px;
    line-height: 24px;
    font-size: 18px;
    width: 20%;
  }
  #main div input{
    height: 18px;
    line-height: 18px;
    font-size: 16px;
    width: 80%;
    outline: none;
  }
  #main div textarea{
    width: 80%;
    height: 120px;
    resize: none;
    outline: none;
    overflow-x: hidden;
    overflow-y: auto;
    word-break: break-all;
    word-wrap: break-word;
  }
  </style>
</head>
<body>
<h1>个人资料</h1>
<div id="main">
<div><a href="/index.php?c=master&a=changelogo" target="_self"><img src="<?=$data['logo']?>" title="点击修改LOGO" alt="点击修改LOGO" /></a></div>
<form action="" method="post">
<div><label for="name">姓名</label><br /><input type="text" name="name" id="name" maxlength="128" value="<?= $data['name'] ?>" /></div>
<div><label for="age">年龄</label><br /><input type="number" name="age" id="age" maxlength="3" value="<?= $data['age'] ?>" /></div>
<div><label for="email">邮箱</label><br /><input type="email" name="email" id="email" maxlength="128" value="<?= $data['email'] ?>" /></div>
<div><label for="qq">QQ</label><br /><input type="number" name="qq" id="qq" maxlength="12" value="<?= $data['qq'] ?>" /></div>
<div><label for="hometown">家乡</label><br /><input type="text" name="hometown" id="hometown" maxlength="128" value="<?= $data['hometown'] ?>" /></div>
<div><label for="skill">技术</label><br /><input type="text" name="skill" id="skill" maxlength="128" value="<?= $data['skill'] ?>" /></div>
<div><label for="height">身高</label><br /><input type="number" name="height" id="height" maxlength="3" value="<?= $data['height'] ?>" /></div>
<div><label for="signature">个性签名</label><br /><input type="text" name="signature" id="signature" maxlength="256" value="<?= $data['signature'] ?>" /></div>
<div><label for="hobby">个人爱好</label><br /><textarea name="hobby" id="hobby"><?= $data['hobby'] ?></textarea></div>
<div><label for="introduce">自我介绍</label><br /><textarea name="introduce" id="introduce"><?= $data['introduce'] ?></textarea></div>
<input type="submit" name="submit" value="确认修改" />
</form>
</div>
</body>
</html>