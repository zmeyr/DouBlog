<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="暮雨秋晨" />
	<title>博主小天地</title>
  <style>
  body,ol,ul,h1,h2,h3,h4,h5,h6,p,th,td,dl,dd,form,fieldset,legend,input,textarea,select{margin:0;padding:0}
body{font:16px "Microsoft Yahei","Arial Narrow",HELVETICA;background:#fff;-webkit-text-size-adjust:100%;}
a{color:inherit;text-decoration:none}
a:hover{color:#cd0200;text-decoration:underline}
em{font-style:normal}
li{list-style:none}
img{border:0;vertical-align:middle}
table{border-collapse:collapse;border-spacing:0}
p{word-wrap:break-word}
  body{
    background-image: url(/view/default/static/pic/bg.png);
  }
  nav{
    text-align: center;
    width: 100%;
    height: 42px;
    line-height: 42px;
    position: fixed;
    top: 0;
    left: 0;
    z-index: 999;
    background-color: black;
  }
  nav ul{
    margin: 0;
    padding: 0;
    list-style-type: none;
  }
  nav ul li{
    width: 25%;
    display: block;
    float: left;
    position: relative;
  }
  nav ul li a,nav ul li a:visited{
    width: 100%;
    height: 42px;
    color: white;
    text-decoration: none;
    display: block;
  }
  nav ul li a:hover{
    color: #cd0200;
    text-decoration: none;
    background-color: #FF9900;
  }
  nav ul li ul{
    display: none;
  }
  nav ul li:hover ul{
    width: 100%;
    position: absolute;
    display: block;
  }
  nav ul li:hover ul li{
    width: 100%;
    height: 42px;
    background-color: black;
    color: white;
    display: block;
  }
  nav ul li:hover ul li a:hover{
    color: #cd0200;
  }
  
  footer{
    color: white;
    text-align: center;
    background-color: black;
    height: 22px;
    line-height: 22px;
    width: 100%;
    position: fixed;
    bottom: 0px;
    left: 0px;
    z-index: 999;
  }
  iframe{
    width: 100%;
    height: 100%;
    position: fixed;
    top: 42px;
    left: 0;
    bottom: 22px;
    overflow: scroll;
    width: 100%;
    border: none;
  }
  </style>
</head>
<body>
<nav>
<ul>
  <li><a href="/index.php?c=master&a=center" target="body">管理中心</a></li>
  <li><a href="#">资源分享</a>
    <ul>
      <li><a href="/index.php?c=master&a=article" target="body">发表文章</a></li>
      <li><a href="/index.php?c=master&a=photo" target="body">分享图片</a></li>
      <li><a href="/index.php?c=master&a=gossip" target="body">发布碎语</a></li>
      <li><a href="/index.php?c=master&a=link" target="body">增加友链</a></li>
    </ul>
  </li>
  <li><a href="#">暂时没定</a></li>
  <li><a href="#">博客管理</a>
    <ul>
      <li><a href="/index.php?c=master&a=config" target="body">博客设置</a></li>
      <li><a href="/index.php?c=master&a=masterinfo" target="body">个人资料</a></li>
      <li><a href="/index.php?c=master&a=changepass" target="body">修改密码</a></li>
      <li><a href="/index.php?c=master&a=logout">退出</a></li>
    </ul>
  </li>
</ul>
</nav>
<iframe name="body" src="/index.php?c=master&a=center"></iframe>
<footer><p>&copy;&nbsp;<a href="htttp://www.doubear.com">Doubear</a> CopyRight All Reserved</p></footer>
</body>
</html>