<?php include 'head.php'; ?>
<body>
<?php include 'header.php'; ?>
<div id="main">
<?php
if (empty($datas)) {
    echo ('<div style="position: absolute;top: 50%;left: 50%;width: 300px;height: 60px;border: 1px solid #eee;background-color: white;margin-top: -30px;margin-left: -150px;text-align: center;"><p style="vertical-align: middle;position: absolute;top: 50%;margin-top: -1.3em;margin-left: 1em;">博主暂时没有添加文章，先去看看其它分类吧~</p></div>');
} else {
?>
<ul>
<?php
    foreach ($datas as $data) {
        $data['content'] = utf8_substr(strip_tags(htmlspecialchars_decode($data['content'])),
            0, 120);
?>
<li style="overflow: hidden !important;"><div class="article">
<h1><a href="/index.php?c=index&a=show&id=<?= $data['id'] ?>"><?= $data['title'] ?></a></h1>
<div class="article_info">
<div class="article_info_head"><span><?= $data['source'] ?></span>|<span><a href="/index.php?c=index&a=index">暮雨秋晨</a></span>|<span><?= date("Y-m-d H:i:s",
        $data['ptime']) ?></span>|<span><?= $data['keywd'] ?></span></div>
<div class="article_info_body"><p><a href="/index.php?c=index&a=show&id=<?= $data['id'] ?>"><?= $data['content'] ?></a>……</p></div>
<div class="article_info_foot"><span style="margin-right: 3px;">点击量：<?= $data['hits'] ?></span><span>&gt;<a href="/index.php?c=index&a=show&id=<?= $data['id'] ?>">阅读全文</a>&lt;</span></span></div>
</div>
</div>
</li>  
<?php
    }
?>
</ul>
<?php } ?>
</div>
<?php include 'footer.php'; ?>

</body>
</html>