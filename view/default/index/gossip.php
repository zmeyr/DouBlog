<?php include 'head.php'; ?>
<body>
<?php include 'header.php'; ?>
<div id="main">
<?php
if (empty($datas)) {
    echo ('<div style="position: absolute;top: 50%;left: 50%;width: 300px;height: 30px;border: 1px solid #eee;background-color: white;margin-top: -15px;margin-left: -150px;text-align: center;"><p style="vertical-align: middle;position: absolute;top: 50%;margin-top: -0.8em;margin-left: 1em;">博主已经懒得吐糟了、各位坐等吧~</p></div>');
} else {
?>
<div class="gossip">
<ul>
<?php foreach ($datas as $data) { ?>
<li><div class="gossip_block"><div style="height: 112px;padding: 3px;overflow-x: hidden;overflow-y: auto;"><?= htmlspecialchars_decode($data['content']) ?></div><div style="height: 16px;border-top: 1px dashed #eee;font-size: 12px;text-align: right;margin-right: 3px;"><?= date("Y-m-d H:i:s",
$data['ptime']) ?></div></div></li>
<?php } ?>
</ul>
</div>
<?php } ?>
</div>
<?php include 'footer.php'; ?>

</body>
</html>