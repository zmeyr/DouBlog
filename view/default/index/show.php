<?php include 'head.php'; ?>
<style>
.show_article_info span{
  margin: 0 3px;
}
.reply_list{
  color: black;
  background-color: white;
}
</style>
<body>
<?php include 'header.php'; ?>
<div id="main" style="margin: 30px auto 44px;">
<h1><?= $data['title'] ?></h1>
<div class="show_article_info" style="margin: 1px; padding: 1px;border: 1px dashed #997;font-size: 12px;"><span><?= $data['source'] ?></span>|<span><a href="/">暮雨秋晨</a></span>|<span><?= date("Y/m/d H:i:s",
$data['ptime']); ?></span>|<span>点击量：<?= $data['hits'] ?></span></div>
<div class="show_article_content" style="margin: 1px; padding: 1px;vertical-align: middle;">
<?= $data['content'] ?>
</div>
<!-- Reply Form Start --!>
<div style="border: 1px dashed #997;clear: both;text-align: center;" class="feedback"><form action="/index.php?c=feedback&a=reply" method="post">
<div style="text-align: left;font-weight: bold;border-bottom: 1px dashed #997;background-color: black;color: white;margin: 1px;">发表看法</div>
<input type="hidden" name="aid" value="<?= $data['id'] ?>" style="display: none;" />
<textarea name="reply" style="text-align: left;outline: none;width: 90%;height: 120px;resize: none;padding: 3px;background-color: white;margin-top: 3px;overflow-x: hidden;overflow-y: auto;" placeholder="最大256个字" onkeyup="if(this.value.length>256){alert('您的输入已经超过系统限制，自动截取前256个字');this.value=this.value.substr(0,256);}"></textarea>
<div style="text-align: left;margin: 1px;border-top: 1px dashed #997;"><span style="margin-left: 3px;">名称：<input type="text" style="text-align: left;padding-left: 3px;outline: none;" maxlength="8" name="name" placeholder="请输入您的名称" /></span><span style="margin-left: 3px;">Email：<input type="email" style="text-align: left;padding-left: 3px;outline: none;" maxlength="64" name="email" placeholder="请输入您的联系邮箱地址" /></span></div>
<div><input type="submit" name="submit" value="提交" style="text-align: center;width: 48px;" /></div>
</form></div>
<!-- Reply List over  --!>
<!-- Reply List Start --!>
<div class="reply_list" style="margin-top: 3px;">
<div style="text-align: center;font-weight: bold;margin: 1px;background-color: black;color: white;">评论列表</div>
<?php foreach ($replys as $reply) { ?>
<div class="reply_list" style="margin: 3px 1px;border: 1px solid #eee;width: 100%;">
<div style="border-bottom: 1px dashed #997;font-size: 12px;background-color: #eee;"><span style="font-size: 16px;font-weight: bold;"><?=$reply['name']?></span>&nbsp;[<span><?=date('Y/m/d H:i:s',$reply['ptime']);?></span>]&nbsp;评论：</div>
<div style="background-color: white;color: black;"><?=$reply['content']?></div>
</div>
<?php } ?>
</div>
<!-- Reply List Over  --!>
</div>
<?php include 'footer.php'; ?>

</body>
</html>