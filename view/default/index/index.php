<?php include 'head.php'; ?>
<body>
<?php include 'header.php'; ?>
<div id="main">
<?php
if (empty($datas['name'])) {
    echo ('<div style="position: absolute;top: 50%;left: 50%;width: 300px;height: 60px;border: 1px solid #eee;background-color: white;margin-top: -30px;margin-left: -150px;text-align: center;"><p style="vertical-align: middle;position: absolute;top: 50%;margin-top: -1.3em;margin-left: 1em;">欢迎使用DouBlog博客系统，请点击右下角连接进入后台添加您的资料吧~</p></div>');
} else {
?>
<div class="master">
<img alt="logo" title="<?= $datas['name'] ?>" class="logo" src="<?= $datas['logo'] ?>" />
<div class="master_infomation">
<h1>博主资料</h1>
<div class="master_name"><span><?= $datas['name'] ?></span></div>
<div class="master_signature">签名：<span><?= $datas['signature'] ?></span></div>
<div class="master_age">年龄：<span><?= $datas['age'] ?></span></div>
<div class="master_height">身高：<span><?= $datas['height'] ?>cm</span></div>
<div class="master_email">邮箱：<span><?= $datas['email'] ?></span></div>
<div class="master_qq">QQ：<span><?= $datas['qq'] ?></span></div>
<div class="master_hometown">家乡：<span><?= $datas['hometown'] ?></span></div>
<div class="master_skill">技能：<span><?= $datas['skill'] ?></span></div>
<div class="master_hobby">个人爱好：<span><?= $datas['hobby'] ?></span></div>
</div>
<div class="master_introduce">
<h2>个人介绍</h2>
<p><?= $datas['introduce'] ?></p>
</div>
</div>
<?php } ?>
</div>
<?php include 'footer.php'; ?>

</body>
</html>